import { PositionList } from "./PositionList";
import { UserForm } from "./UserForm";
import { UserList } from "./UserList";

const App = () => {
  return (
    <div className="mx-48 pt-6 pb-48">
      <h1 className="font-black text-3xl text-center mb-16">
        ABZ Node.js Developer Test Assignment
      </h1>
      <div className="flex justify-center gap-40">
        <UserForm />
        <PositionList />
      </div>
      <UserList />
    </div>
  );
};

export default App;
