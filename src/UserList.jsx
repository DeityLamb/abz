import { createResource, createSignal, For } from "solid-js";
import { API_URL } from "./constants";

export function UserList() {
  const [page, setPage] = createSignal(`${API_URL}/users`);

  const [response] = createResource(page, (page) =>
    fetch(page)
      .then((v) => v.json())
      .catch(() => null)
  );

  return (
    <div>
      <div className="mt-24 flex flex-col items-center">
        <h2 className="text-2xl mb-6 font-bold">List of Users</h2>
        <div className="overflow-x-auto">
          <table className="table min-w-max">
            <thead>
              <tr>
                <th></th>
                <th>Name</th>
                <th>Position</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Registered At</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <For each={response()?.users}>
                {(user) => <User user={user} />}
              </For>
            </tbody>
          </table>
        </div>
        <div className="join">
          <button
            disabled={!response()?.links?.prev_url}
            onClick={() => setPage(response()?.links?.prev_url)}
            className="join-item btn"
          >
            «
          </button>
          <span className="join-item btn no-animation">
            page {response()?.page} / {response()?.total_pages}
          </span>
          <button
            disabled={!response()?.links?.next_url}
            onClick={() => setPage(response()?.links?.next_url)}
            className="join-item btn"
          >
            »
          </button>
        </div>
      </div>
    </div>
  );
}

function User({ user }) {
  return (
    <tr>
      <th>{user.id}</th>
      <td>{user.name}</td>
      <td>{user.position}</td>
      <td>{user.email}</td>
      <td>{user.phone}</td>
      <td>{new Date(user.registration_timestamp * 1000).toDateString()}</td>
      <td>
        <img height={70} width={70} src={user.photo} alt="photo" />
      </td>
    </tr>
  );
}
