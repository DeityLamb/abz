export const API_URL =
  import.meta.env.MODE === "evelopment"
    ? "http://localhost:3000"
    : "https://abz-api.deitylamb.me";