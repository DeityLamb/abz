import { createResource } from "solid-js";
import { API_URL } from "./constants";

export function UserForm() {
  const [token, { refetch, mutate }] = createResource(() =>
    fetch(`${API_URL}/token`)
      .then((r) => r.json())
      .then((v) => v.token)
  );

  /**
   *
   * @param {FormDataEvent} event
   */
  function sumbit(event) {
    event.preventDefault();
    event.stopPropagation();

    console.log(event);

    fetch(`${API_URL}/users`, {
      method: "POST",
      body: new FormData(event.target),
      headers: {
        Token: token(),
      },
    });
  }

  return (
    <div className="mt-16 flex flex-col items-center justify-center">
      <div className="w-80">
        <h2 className="text-2xl font-bold">Add new user</h2>
        <form onSubmit={(e) => sumbit(e)} className="flex flex-col">
          <label className="mt-10 mb-4 input input-bordered flex items-center gap-2">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              fill="currentColor"
              className="w-4 h-4 opacity-70"
            >
              <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6ZM12.735 14c.618 0 1.093-.561.872-1.139a6.002 6.002 0 0 0-11.215 0c-.22.578.254 1.139.872 1.139h9.47Z" />
            </svg>
            <input
              type="text"
              name="name"
              className="grow"
              placeholder="Name"
            />
          </label>
          <label className="mb-4 input input-bordered flex items-center gap-2">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              fill="currentColor"
              className="w-4 h-4 opacity-70"
            >
              <path d="M2.5 3A1.5 1.5 0 0 0 1 4.5v.793c.026.009.051.02.076.032L7.674 8.51c.206.1.446.1.652 0l6.598-3.185A.755.755 0 0 1 15 5.293V4.5A1.5 1.5 0 0 0 13.5 3h-11Z" />
              <path d="M15 6.954 8.978 9.86a2.25 2.25 0 0 1-1.956 0L1 6.954V11.5A1.5 1.5 0 0 0 2.5 13h11a1.5 1.5 0 0 0 1.5-1.5V6.954Z" />
            </svg>
            <input
              type="text"
              name="email"
              className="grow"
              placeholder="Email"
            />
          </label>
          <label className="mb-4 input input-bordered flex items-center gap-2">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              fill="currentColor"
              className="w-4 h-4 opacity-70"
            >
              <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6ZM12.735 14c.618 0 1.093-.561.872-1.139a6.002 6.002 0 0 0-11.215 0c-.22.578.254 1.139.872 1.139h9.47Z" />
            </svg>
            <input
              type="text"
              name="phone"
              className="grow"
              placeholder="Phone"
            />
          </label>
          <label className="mb-4 input input-bordered flex items-center gap-2">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 16 16"
              fill="currentColor"
              className="w-4 h-4 opacity-70"
            >
              <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6ZM12.735 14c.618 0 1.093-.561.872-1.139a6.002 6.002 0 0 0-11.215 0c-.22.578.254 1.139.872 1.139h9.47Z" />
            </svg>
            <input
              type="number"
              name="position_id"
              className="grow"
              placeholder="Position id"
            />
          </label>
          <label className="mb-4 form-control w-full max-w-xs">
            <div className="label">
              <span className="label-text-alt">Pick a photo</span>
            </div>
            <input
              type="file"
              name="photo"
              className="file-input file-input-md file-input-bordered w-full max-w-xs"
            />
          </label>
          <div className="mb-10 flex flex-col">
            <span className="mb-2 label-text-alt">Token</span>
            <div className="flex">
              <input
                onChange={(e) => mutate(e.target.value)}
                className="input input-bordered w-80 overflow-x-scroll grow"
                value={token() ?? ""}
              ></input>
              <button onClick={refetch} class="btn ml-2 rounded-sm">
                Regenerate
              </button>
            </div>
          </div>
          <input
            type="submit"
            value="Submit"
            className="btn btn-primary mx-auto w-full"
          />
        </form>
      </div>
    </div>
  );
}
