import { createResource, For } from "solid-js";
import { API_URL } from "./constants";

export function PositionList() {
  const [positions] = createResource(() =>
    fetch(`${API_URL}/positions`)
      .then((v) => v.json())
      .then((v) => v.positions)
      .catch(() => null)
  );

  return (
    <div>
      <div className="mt-16 flex flex-col items-center">
        <h2 className="text-2xl mb-6 font-bold">List of Positions</h2>
        <div className="overflow-x-auto">
          <table className="table min-w-max">
            <thead>
              <tr>
                <th>Id </th>
                <th>Position name</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <For each={positions()}>
                {(position) => (
                  <tr>
                    <th>{position.id}</th>
                    <td>{position.name}</td>
                  </tr>
                )}
              </For>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
